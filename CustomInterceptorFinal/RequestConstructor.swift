//
//  RequestConstructor.swift
//  CustomInterceptorFinal
//
//  Created by Aynur Asadova on 10.11.21.
//

import Foundation

class RequestConstructor {
   
   let baseUrl: URL
   let queryParameters: [String: String]?
   let httpBody: [String: Any]?
   let httpHeaders: [String: String]?
   let httpMethod: HTTPMethod
   
   init(baseUrl: URL, parameters: [String: String]? = nil, body: [String: Any]? = nil, headers: [String: String]? = nil, method: HTTPMethod = .get) {
       self.baseUrl = baseUrl
       self.queryParameters = parameters
       self.httpBody = body
       self.httpHeaders = headers
       self.httpMethod = method
   }
   
   func constructRequest() -> URLRequest {
       
       var url: URL = baseUrl
       
       if let params = queryParameters {
           let query = QueryHandler(params)
           url = query.constructQuery(with: baseUrl)!
       }
       
       var urlRequest = URLRequest(url: url)
       urlRequest.httpMethod = httpMethod.rawValue
       urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
       
       if let httpBody = httpBody {
           let bodyConstructor = BodyConstructor(httpBody)
           urlRequest.httpBody = bodyConstructor.getHttpBody
       }
       
       if let headers = httpHeaders {
           urlRequest.allHTTPHeaderFields = headers
       }
       return urlRequest
   }
}

class QueryHandler {
   
   //MARK: - Variables
   private var parameters: [String: String]
  
   
   init(_ parameters: [String: String])  {
       self.parameters = parameters
   }
   func constructQuery(with url: URL) -> URL? {
       var urlComponent = URLComponents(string: url.absoluteString)
       var query = [URLQueryItem(name: "", value: nil)]

       parameters.forEach { (key, val) in
           let item = URLQueryItem(name: key, value: val)
           query.append(item)
       }
       
       urlComponent?.queryItems = query
       
       return urlComponent?.url
   }
   
}

class BodyConstructor {
   
   private let body: [String: Any]
   private var httpBody: Data? = nil
   
   
   init(_ body: [String: Any]) {
       self.body = body
       print(body)
   }
  
   var getHttpBody: Data? {
       get {
           do {
               httpBody = try JSONSerialization.data(withJSONObject: body, options: .prettyPrinted)
           } catch let err {
               print(err.localizedDescription)
           }
           return httpBody
       }
   }
}
