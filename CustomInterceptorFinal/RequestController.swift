//
//  RequestController.swift
//  CustomInterceptorFinal
//
//  Created by Aynur Asadova on 09.11.21.
//

import UIKit


typealias ResponseInterceptor = (URLRequest?, URLResponse?) -> Void
typealias RequestInterceptor = (URLRequest) -> URLRequest

class RequestController: NSObject, URLSessionDelegate {
    
    static let shared = RequestController()
    
    private override init() {}
    
    var responseInterceptors: Array<ResponseInterceptor> = []
    var requestInterceptors: Array<RequestInterceptor> = []
    
    func addResponseInterceptor(interceptor: @escaping ResponseInterceptor) {
        self.responseInterceptors.append(interceptor)
    }
    
    func addRequestInterceptor(interceptor: @escaping RequestInterceptor) {
        self.requestInterceptors.append(interceptor)
    }
    
    func send(_ url: URL, params: [String: String]? = nil, body: [String: Any]? = nil, headers: [String: String]? = nil, method: HTTPMethod, completion: @escaping (Data? ,URLResponse?, Error?) -> Void) -> Int {
        let reqConstructor = RequestConstructor(baseUrl: url, parameters: params, body: body, headers: headers, method: method)
        var request = reqConstructor.constructRequest()
        
        for i in self.requestInterceptors {
            request = i(request)
        }
        
        let task = DataTaskManager(with: request)
        let taskId = task.fireSession(delegate: self, completionHandler: completion, responseInterceptors: self.responseInterceptors)
        
        return taskId
    }
    
    func sendWithRequest(request: URLRequest, completion: @escaping (Data? ,URLResponse?, Error?) -> Void) -> Int {
        var newRequest = request
        for i in self.requestInterceptors {
            newRequest = i(request)
        }
        
        let task = DataTaskManager(with: newRequest)
        let taskId = task.fireSession(delegate: self, completionHandler: completion, responseInterceptors: self.responseInterceptors)
        
        return taskId
    }
    
}
