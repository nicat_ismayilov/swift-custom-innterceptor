//
//  DataTaskManager.swift
//  CustomInterceptorFinal
//
//  Created by Aynur Asadova on 10.11.21.
//

import Foundation

protocol TaskManager {
    func fireSession(delegate: URLSessionDelegate?, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void, responseInterceptors:  Array<ResponseInterceptor>?) -> Int
    
}

 class DataTaskManager: TaskManager {
    private var urlRequest: URLRequest
    
    init(with urlRequest: URLRequest) {
        self.urlRequest = urlRequest
    }
    
    
    func fireSession(delegate: URLSessionDelegate?, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void, responseInterceptors: Array<ResponseInterceptor>?) -> Int {
        let session = URLSession(configuration: .ephemeral, delegate: delegate, delegateQueue: .main)
        let task = session.dataTask(with: urlRequest) { [self] data, response, error in
            if let responseInterceptors = responseInterceptors {
                for i in responseInterceptors {
                    i(self.urlRequest, response)
                }
            }
            
            completionHandler(data, response, error)
        }
        
        task.resume()
        
        return task.taskIdentifier
    }
     
}
