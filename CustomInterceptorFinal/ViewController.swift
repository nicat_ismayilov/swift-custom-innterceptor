//
//  ViewController.swift
//  CustomInterceptorFinal
//
//  Created by Aynur Asadova on 09.11.21.
//

import UIKit

class ViewController: UIViewController {
    var failedRequests: [URLRequest] = []
    var isGettingNewToken = false
    var accessToken: String = "Old Token"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        RequestController.shared.addRequestInterceptor { request in
            var newRequest = request
            newRequest.setValue(self.accessToken, forHTTPHeaderField: "Authorization")
            
            return newRequest
        }
        
        RequestController.shared.addResponseInterceptor { request, response in
            guard let request = request, let response = response as? HTTPURLResponse else {
                return
            }
            
            if response.statusCode == 401 {
                self.failedRequests.append(request)
                
                if (!self.isGettingNewToken) {
                    self.accessToken = ""
                    self.isGettingNewToken = true
                    let newAccessToken = self.getNewAccessToken()
                    self.retryFailedRequests(accessToken: newAccessToken)
                }
            }
            
            print("This is from response interceptor \(request.allHTTPHeaderFields)");
        }
    }
    
    func retryFailedRequests(accessToken: String) {
        self.accessToken = accessToken
        
        for var request in self.failedRequests {
            request.url = URL(string: "https://mock.codes/200")
            
            let _ = RequestController.shared.sendWithRequest(request: request) { data, response, error in
                let str = String(decoding: data!, as: UTF8.self)
                print("send \(str)")
            }
        }
        
//        self.failedRequests.forEach { request in
//            request.url = URL(string: "https://mock.codes/200")
//
//            let _ = RequestController.shared.sendWithRequest(request: request) { data, response, error in
//                let str = String(decoding: data!, as: UTF8.self)
//                print("send \(str)")
//            }
//        }
    }
    
    @objc func getNewAccessToken() -> String {
        self.isGettingNewToken = false
        
        return "New Access Token"
    }
    
    @IBAction func buttonClicked(_ sender: UIButton) {
        
//        let headers = [
//            "accessToken": "Old Token"
//        ]
        
        let _ = RequestController.shared.send(URL(string: "https://jsonplaceholder.typicode.com/posts/1")!, params: nil, body: nil, headers: nil, method: .get) { data, response, error in
            let str = String(decoding: data!, as: UTF8.self)
            print("send \(str)")
        }
        
        let _ = RequestController.shared.send(URL(string: "https://mock.codes/401")!, params: nil, body: nil, headers: nil, method: .get) { data, response, error in
            let str = String(decoding: data!, as: UTF8.self)
            print("send \(str)")
        }
    }
}

