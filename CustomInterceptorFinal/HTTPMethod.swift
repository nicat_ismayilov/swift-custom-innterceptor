//
//  HTTPMethod.swift
//  CustomInterceptorFinal
//
//  Created by Aynur Asadova on 09.11.21.
//


import Foundation


public enum HTTPMethod: String {
    case post = "POST"
    case get = "GET"
    case delete = "DELETE"
    case put = "PUT"
}
